package com.kyjg.patientmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VisitText {
    FIRST_VISIT("초진"),
    REVISIT("재진");

    private final String name;
}
