package com.kyjg.patientmanager.entity;

import com.kyjg.patientmanager.enums.VisitText;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String patientName;
    @Column(nullable = false, length = 20)
    private String patientPhone;
    @Column(nullable = false, length = 50)
    private String patientAddress;
    @Column(nullable = false, length = 30)
    private String diseaseName;
    @Column(nullable = false)
    private LocalDate visitDate;
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private VisitText visitText;

    private LocalDate lastVisitDate;
}
