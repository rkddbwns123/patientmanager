package com.kyjg.patientmanager.controller;

import com.kyjg.patientmanager.model.PatientInfoUpdateRequest;
import com.kyjg.patientmanager.model.PatientItem;
import com.kyjg.patientmanager.model.PatientRequest;
import com.kyjg.patientmanager.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/patient")
public class PatientController {
    private final PatientService patientService;
    @PostMapping("/data")
    public String setPatient(@RequestBody @Valid PatientRequest request) {
        patientService.setPatient(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<PatientItem> getPatient() {
        List<PatientItem> result = patientService.getPatient();

        return result;
    }
    @PutMapping("/info/id/{id}")
    public String putInfoPatient(@PathVariable long id, @RequestBody PatientInfoUpdateRequest request) {
        patientService.putInfoPatient(id, request);

        return "OK";
    }
    @PutMapping("/visit/id/{id}")
    public String putVisitPatient(@PathVariable long id) {
        patientService.putVisitPatient(id);

        return "OK";
    }
    @DeleteMapping("/del-info/id/{id}")
    public String delPatient(@PathVariable long id) {
        patientService.delPatient(id);

        return "OK";
    }
}
