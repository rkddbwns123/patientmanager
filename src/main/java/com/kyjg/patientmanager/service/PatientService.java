package com.kyjg.patientmanager.service;

import com.kyjg.patientmanager.entity.Patient;
import com.kyjg.patientmanager.enums.VisitText;
import com.kyjg.patientmanager.model.PatientInfoUpdateRequest;
import com.kyjg.patientmanager.model.PatientItem;
import com.kyjg.patientmanager.model.PatientRequest;
import com.kyjg.patientmanager.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;

    public void setPatient(PatientRequest request) {
        Patient addData = new Patient();

        addData.setPatientName(request.getPatientName());
        addData.setPatientPhone(request.getPatientPhone());
        addData.setPatientAddress(request.getPatientAddress());
        addData.setDiseaseName(request.getDiseaseName());
        addData.setVisitDate(LocalDate.now());
        addData.setVisitText(VisitText.FIRST_VISIT);

        patientRepository.save(addData);
    }

    public List<PatientItem> getPatient() {
        List<Patient> originList = patientRepository.findAll();

        List<PatientItem> result = new LinkedList<>();

        for (Patient item : originList) {
            PatientItem addData = new PatientItem();

            addData.setId(item.getId());
            addData.setPatientName(item.getPatientName());
            addData.setPatientPhone(item.getPatientPhone());
            addData.setPatientAddress(item.getPatientAddress());
            addData.setDiseaseName(item.getDiseaseName());
            addData.setVisitText(item.getVisitText());

            if (!item.getVisitText().equals(VisitText.FIRST_VISIT)) {
                addData.setLastVisitDate(item.getVisitDate());
            }

            result.add(addData);
        }
        return result;
    }
    public void putInfoPatient(long id, PatientInfoUpdateRequest request) {
        Patient originData = patientRepository.findById(id).orElseThrow();

        originData.setPatientPhone(request.getPatientPhone());
        originData.setPatientAddress(request.getPatientAddress());

        patientRepository.save(originData);
    }
    public void putVisitPatient(long id) {
        Patient originData = patientRepository.findById(id).orElseThrow();

        originData.setLastVisitDate(originData.getVisitDate());
        originData.setVisitDate(LocalDate.now());

        if (LocalDate.now().isAfter(originData.getVisitDate())); {
            originData.setVisitText(VisitText.REVISIT);
        }
        patientRepository.save(originData);
    }
    public void delPatient(long id) {
        patientRepository.deleteById(id);
    }
}
