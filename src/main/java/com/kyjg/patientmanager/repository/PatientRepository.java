package com.kyjg.patientmanager.repository;

import com.kyjg.patientmanager.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
}
