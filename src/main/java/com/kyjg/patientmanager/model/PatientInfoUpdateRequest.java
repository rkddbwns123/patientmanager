package com.kyjg.patientmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PatientInfoUpdateRequest {
    @NotNull
    @Length(min = 11, max = 20)
    private String patientPhone;
    @NotNull
    @Length(min = 2, max = 50)
    private String patientAddress;
}
