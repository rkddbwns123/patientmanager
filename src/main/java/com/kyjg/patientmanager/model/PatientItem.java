package com.kyjg.patientmanager.model;

import com.kyjg.patientmanager.enums.VisitText;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Getter
@Setter
public class PatientItem {
    private Long id;

    private String patientName;

    private String patientPhone;

    private String patientAddress;

    private String diseaseName;
    @Enumerated(value = EnumType.STRING)
    private VisitText visitText;

    private LocalDate lastVisitDate;
}
