package com.kyjg.patientmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PatientRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String patientName;
    @NotNull
    @Length(min = 11, max = 20)
    private String patientPhone;
    @NotNull
    @Length(min = 2, max = 50)
    private String patientAddress;
    @NotNull
    @Length(min = 1, max = 30)
    private String diseaseName;
}
